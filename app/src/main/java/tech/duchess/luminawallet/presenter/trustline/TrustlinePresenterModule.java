package tech.duchess.luminawallet.presenter.trustline;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import tech.duchess.luminawallet.dagger.SchedulerProvider;
import tech.duchess.luminawallet.model.api.HorizonApi;
import tech.duchess.luminawallet.model.repository.AccountRepository;
import tech.duchess.luminawallet.model.repository.FeesRepository;

@Module
public class TrustlinePresenterModule {
    @Provides
    TrustlineContract.TrustlinePresenter provideTrustlinePresenter(TrustlineContract.TrustlineView trustlineView,
                                                                   OkHttpClient okHttpClient,
                                                                   HorizonApi horizonApi,
                                                                   AccountRepository accountRepository,
                                                                   FeesRepository feesRepository,
                                                                   SchedulerProvider schedulerProvider) {
        return new TrustlinePresenter(trustlineView, okHttpClient, horizonApi, accountRepository,
                feesRepository, schedulerProvider);
    }
}
