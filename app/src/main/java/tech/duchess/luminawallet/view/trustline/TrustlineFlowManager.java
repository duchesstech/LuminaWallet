package tech.duchess.luminawallet.view.trustline;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import io.reactivex.functions.Action;

public interface TrustlineFlowManager {
    void showLoading(boolean isLoading);
    void showBlockedLoading(@Nullable String message);
    void hideBlockedLoading(@Nullable String message,
                            boolean wasSuccess,
                            boolean immediate,
                            @Nullable Action action);
    void setTitle(@NonNull String title);
}
