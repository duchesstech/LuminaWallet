package tech.duchess.luminawallet.view.account.receive;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import tech.duchess.luminawallet.R;
import tech.duchess.luminawallet.model.persistence.account.Account;
import tech.duchess.luminawallet.view.account.AccountPerspectiveView;
import tech.duchess.luminawallet.view.util.TextUtils;
import tech.duchess.luminawallet.view.util.ViewUtils;

public class ReceiveFragment extends Fragment implements AccountPerspectiveView {
    private static final String ACCOUNT_KEY = "ReceiveFragment.ACCOUNT_KEY";
    private static final String ADDRESS_KEY = "ReceiveFragment.ADDRESS_KEY";

    @BindView(R.id.qr_code)
    ImageView qrCode;

    @BindView(R.id.address)
    TextView addressField;

    @BindView(R.id.address_container)
    View addressContainer;

    private String address;
    private Unbinder unbinder;

    public static ReceiveFragment newInstance(@Nullable Account account) {
        Bundle args = new Bundle();
        args.putParcelable(ACCOUNT_KEY, account);
        ReceiveFragment fragment = new ReceiveFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.receive_fragment, container, false);
        unbinder = ButterKnife.bind(this, view);

        if (savedInstanceState == null) {
            Bundle args = getArguments();
            setAccount(args == null ? null : args.getParcelable(ACCOUNT_KEY));
        } else {
            setAddress(savedInstanceState.getString(ADDRESS_KEY));
        }

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(ADDRESS_KEY, address);
    }

    @Override
    public void setAccount(@Nullable Account account) {
        setAddress(account == null ? null : account.getAccount_id());
    }

    @Override
    public void transactionPostedForAccount(@NonNull Account account) {
        // No-op
    }

    private void setAddress(@Nullable String address) {
        if (!TextUtils.isEmpty(this.address) && this.address.equals(address)) {
            // No change in address (which is the only data we reflect corresponding to an account),
            // thus no updates necessary.
            return;
        }

        if (TextUtils.isEmpty(address)) {
            qrCode.setImageDrawable(null);
            qrCode.setVisibility(View.INVISIBLE);
            addressField.setText("");
            addressContainer.setVisibility(View.INVISIBLE);
        } else {
            qrCode.setVisibility(View.VISIBLE);
            qrCode.setImageBitmap(ViewUtils.encodeAsBitmap(address));
            addressContainer.setVisibility(View.VISIBLE);
            addressField.setText(address);
        }
    }

    @OnClick(R.id.btn_copy_address)
    public void onUserRequestCopyAddress() {
        Context context = getContext();
        if (context == null) {
            return;
        }
        CharSequence contactAddress = addressField.getText();

        String toastMessage;
        if (ViewUtils.copyToClipboard(context, getString(R.string.address_clipboard_label),
                contactAddress)) {
            toastMessage = getString(R.string.address_copied_success_toast);
        } else {
            toastMessage = getString(R.string.address_copied_failed_toast);
        }

        Toast.makeText(context, toastMessage, Toast.LENGTH_SHORT).show();
    }

    private void copyAddressToClipboard() {
        Context context = getContext();

        String toastMessage;
        if (context != null
                && ViewUtils.copyToClipboard(getContext(),
                getString(R.string.address_clipboard_label),
                address)) {
            toastMessage = getString(R.string.address_copied_success_toast);
        } else {
            toastMessage = getString(R.string.address_copied_failed_toast);
        }

        Toast.makeText(context, toastMessage, Toast.LENGTH_SHORT).show();
    }
}
