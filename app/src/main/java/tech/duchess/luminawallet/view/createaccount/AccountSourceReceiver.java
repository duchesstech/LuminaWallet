package tech.duchess.luminawallet.view.createaccount;

public interface AccountSourceReceiver {
    void onUserRequestedAccountCreation(boolean isImportingSeed);
}
