package tech.duchess.luminawallet.view.trustline;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.text.DecimalFormat;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import tech.duchess.luminawallet.R;
import tech.duchess.luminawallet.model.util.AssetUtil;
import tech.duchess.luminawallet.model.util.FeesUtil;
import tech.duchess.luminawallet.presenter.trustline.TrustlineContract.CommonTrustline;
import tech.duchess.luminawallet.presenter.trustline.TrustlineContract.FederatedTrustlineChoice;
import tech.duchess.luminawallet.presenter.trustline.TrustlineContract.TrustlinePresenter.TrustlineRequestError;
import tech.duchess.luminawallet.view.common.BaseFragment;
import tech.duchess.luminawallet.view.util.TextUtils;
import tech.duchess.luminawallet.view.util.ViewUtils;
import timber.log.Timber;

public class TrustlineConfirmationFragment extends BaseFragment {
    private static final String CREATION_TYPE_ARG = "TrustlineConfirmationFragment.CREATION_TYPE_ARG";
    private static final String COMMON_ASSET_ARG = "TrustlineConfirmationFragment.COMMON_ASSET_ARG";
    private static final String FEDERATED_TRUSTLINE_CHOICE_ARG = "TrustlineConfirmationFragment.FEDERATED_TRUSTLINE_CHOICE_ARG";
    private static final String FEE_ARG = "TrustlineConfirmationFragment.FEE_ARG";
    private static final DecimalFormat LIMIT_FORMATTER = new DecimalFormat("##############");

    static {
        LIMIT_FORMATTER.setMaximumFractionDigits(7);
    }

    @BindView(R.id.asset_code_field_layout)
    TextInputLayout assetCodeFieldLayout;

    @BindView(R.id.asset_code_field)
    TextInputEditText assetCodeField;

    @BindView(R.id.trust_amount_field_layout)
    TextInputLayout trustAmountFieldLayout;

    @BindView(R.id.trust_amount_field)
    TextInputEditText trustAmountField;

    @BindView(R.id.issuer_field_layout)
    TextInputLayout issuerFieldLayout;

    @BindView(R.id.issuer_field)
    TextInputEditText issuerField;

    @BindView(R.id.password_field_layout)
    TextInputLayout passwordFieldLayout;

    @BindView(R.id.password_field)
    TextInputEditText passwordField;

    @BindView(R.id.transaction_fee)
    TextView transactionFee;

    @BindView(R.id.confirm_button)
    Button confirmButton;

    public static TrustlineConfirmationFragment newInstanceManualInput(double fee) {
        Bundle args = new Bundle();
        args.putString(CREATION_TYPE_ARG, CreationType.MANUAL.name());
        args.putDouble(FEE_ARG, fee);
        return setArgs(args);
    }

    public static TrustlineConfirmationFragment newInstanceCommonAsset(@NonNull CommonTrustline commonTrustline, double fee) {
        Bundle args = new Bundle();
        args.putString(CREATION_TYPE_ARG, CreationType.COMMON.name());
        args.putString(COMMON_ASSET_ARG, commonTrustline.name());
        args.putDouble(FEE_ARG, fee);
        return setArgs(args);
    }

    public static TrustlineConfirmationFragment newInstanceFederatedTrustlineChoice(@NonNull FederatedTrustlineChoice trustlineChoice, double fee) {
        Bundle args = new Bundle();
        args.putString(CREATION_TYPE_ARG, CreationType.FEDERATION_CHOICE.name());
        args.putParcelable(FEDERATED_TRUSTLINE_CHOICE_ARG, trustlineChoice);
        args.putDouble(FEE_ARG, fee);
        return setArgs(args);
    }

    private static TrustlineConfirmationFragment setArgs(@NonNull Bundle args) {
        TrustlineConfirmationFragment fragment = new TrustlineConfirmationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private enum CreationType {
        MANUAL,
        COMMON,
        FEDERATION_CHOICE
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.trustline_confirmation_fragment, container,
                false);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);

        if (savedInstanceState == null) {
            // First launch, check creation origin.

            Bundle args = getArguments();

            if (args == null) {
                Timber.e("Bundle was null!");
                return;
            }

            CreationType creationType = CreationType.valueOf(args.getString(CREATION_TYPE_ARG));

            switch (creationType) {
                case COMMON:
                    CommonTrustline commonTrustline =
                            CommonTrustline.valueOf(args.getString(COMMON_ASSET_ARG));
                    assetCodeField.setText(commonTrustline.getAssetCode());
                    trustAmountField.setText(LIMIT_FORMATTER.format(commonTrustline.getLimit()));
                    issuerField.setText(commonTrustline.getIssuer());
                    break;
                case FEDERATION_CHOICE:
                    FederatedTrustlineChoice choice =
                            args.getParcelable(FEDERATED_TRUSTLINE_CHOICE_ARG);
                    if (choice != null) {
                        assetCodeField.setText(choice.assetCode);
                        issuerField.setText(choice.assetIssuer);
                    }
                    break;
            }

            double fee = args.getDouble(FEE_ARG, .00001);
            transactionFee.setText(FeesUtil.getFeeString(getResources(), fee));
        }
    }

    @OnTextChanged(R.id.password_field)
    public void onUserChangedPassword(Editable editable) {
        passwordFieldLayout.setError(null);

        if (!confirmButton.isEnabled()) {
            confirmButton.setEnabled(true);
        }
    }

    @OnTextChanged(R.id.asset_code_field)
    public void onUserChangedAsset(Editable editable) {
        assetCodeFieldLayout.setError(null);
    }

    @OnTextChanged(R.id.issuer_field)
    public void onUserChangedIssuer(Editable editable) {
        issuerFieldLayout.setError(null);
    }

    @OnTextChanged(R.id.trust_amount_field)
    public void onUserChangedTrust(Editable editable) {
        trustAmountFieldLayout.setError(null);
    }

    @OnClick(R.id.confirm_button)
    public void onUserConfirmedTransaction() {
        ViewUtils.whenNonNull(getParentFragment(), parentFragment -> {
            if (parentFragment instanceof TrustlineConfirmationListener) {
                ((TrustlineConfirmationListener) parentFragment)
                        .onTrustlineConfirmed(
                                issuerField.getText().toString(),
                                assetCodeField.getText().toString(),
                                trustAmountField.getText().toString(),
                                passwordField.getText().toString());
            } else {
                Timber.e("Parent fragment wasn't listening for trustline confirmation");
            }
        });
    }

    @OnClick(R.id.cancel_button)
    public void onUserCanceledTransaction() {
        dismiss();
    }

    @OnClick(R.id.take_picture)
    public void onUserRequestQRScanner() {
        IntentIntegrator intentIntegrator = IntentIntegrator.forSupportFragment(this);
        intentIntegrator.setBeepEnabled(false);
        intentIntegrator.setBarcodeImageEnabled(false);
        intentIntegrator.initiateScan();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (!TextUtils.isEmpty(result.getContents())) {
                issuerField.setText(result.getContents());
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void showError(@NonNull TrustlineRequestError trustlineRequestError) {
        switch (trustlineRequestError) {
            case PASSWORD_LENGTH:
                passwordFieldLayout.setError(getString(R.string.password_length_error));
                break;
            case ASSET_CODE_INVALID:
                assetCodeFieldLayout.setError(getString(R.string.asset_code_invalid_error));
                break;
            case ISSUER_ADDRESS_PREFIX:
                issuerFieldLayout.setError(getString(R.string.issuer_address_prefix_error));
                break;
            case ISSUER_ADDRESS_LENGTH:
                issuerFieldLayout.setError(getString(R.string.issuer_address_length_error,
                        getResources().getInteger(R.integer.address_length)));
                break;
            case ISSUER_ADDRESS_FORMAT:
                issuerFieldLayout.setError(getString(R.string.issuer_address_invalid_format_error));
                break;
            case TRUST_AMOUNT_INVALID:
                trustAmountFieldLayout.setError(getString(R.string.trust_amount_invalid_error));
                break;
        }
    }

    public interface TrustlineConfirmationListener {
        void onTrustlineConfirmed(@Nullable String issuer,
                                  @Nullable String assetCode,
                                  @Nullable String limit,
                                  @Nullable String password);
    }
}
