package tech.duchess.luminawallet.view.trustline;

import android.support.v4.app.Fragment;

import javax.inject.Named;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import tech.duchess.luminawallet.dagger.module.BaseFragmentModule;
import tech.duchess.luminawallet.dagger.scope.PerChildFragment;
import tech.duchess.luminawallet.dagger.scope.PerFragment;
import tech.duchess.luminawallet.presenter.trustline.TrustlineContract;
import tech.duchess.luminawallet.presenter.trustline.TrustlinePresenterModule;

@Module(includes = {BaseFragmentModule.class, TrustlinePresenterModule.class})
abstract class TrustlineFragmentModule {
    @Binds
    @Named(BaseFragmentModule.FRAGMENT)
    @PerFragment
    abstract Fragment fragment(TrustlineFragment trustlineFragment);

    @Binds
    @PerFragment
    abstract TrustlineContract.TrustlineView provideTrustlineView(TrustlineFragment trustlineFragment);

    @PerChildFragment
    @ContributesAndroidInjector(modules = TrustlineConfirmationFragmentModule.class)
    abstract TrustlineConfirmationFragment trustlineConfirmationFragmentInjector();
}
