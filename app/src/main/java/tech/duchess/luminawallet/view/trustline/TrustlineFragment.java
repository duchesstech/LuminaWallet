package tech.duchess.luminawallet.view.trustline;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.leinardi.android.speeddial.SpeedDialActionItem;
import com.leinardi.android.speeddial.SpeedDialView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import tech.duchess.luminawallet.R;
import tech.duchess.luminawallet.model.util.AssetUtil;
import tech.duchess.luminawallet.model.util.FeesUtil;
import tech.duchess.luminawallet.presenter.trustline.TrustlineContract;
import tech.duchess.luminawallet.presenter.trustline.TrustlineContract.CommonTrustline;
import tech.duchess.luminawallet.presenter.trustline.TrustlineContract.FederatedTrustlineChoice;
import tech.duchess.luminawallet.presenter.trustline.TrustlineContract.Trustline;
import tech.duchess.luminawallet.presenter.trustline.TrustlineContract.TrustlinePresenter.TrustlineRemoveError;
import tech.duchess.luminawallet.presenter.trustline.TrustlineContract.TrustlinePresenter.TrustlineRequestError;
import tech.duchess.luminawallet.view.common.BaseViewFragment;
import tech.duchess.luminawallet.view.util.ViewUtils;
import timber.log.Timber;

public class TrustlineFragment extends BaseViewFragment<TrustlineContract.TrustlinePresenter>
        implements TrustlineContract.TrustlineView,
        TrustlineConfirmationFragment.TrustlineConfirmationListener,
        TrustlineActivity.RefreshListener {
    private static final String ACCOUNT_ID_ARG = "TrustlineFragment.ACCOUNT_ID_ARG";
    private static final Comparator<Trustline> TRUSTLINE_COMPARATOR = (o1, o2) ->
            o1.getAssetCode().compareToIgnoreCase(o2.getAssetCode());

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.no_trustlines_message)
    TextView noTrustlinesMessage;

    @BindView(R.id.speed_dial)
    SpeedDialView speedDialView;

    private TrustlineRecyclerAdapter adapter;
    private AlertDialog currentDialog;

    static TrustlineFragment getInstance(@Nullable String accountId) {
        Bundle args = new Bundle();
        args.putString(ACCOUNT_ID_ARG, accountId);
        TrustlineFragment fragment = new TrustlineFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((TrustlineFlowManager) activityContext).setTitle(getString(R.string.trustline_fragment_title));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        dismissCurrentDialog();
    }

    private void dismissCurrentDialog() {
        if (currentDialog != null && currentDialog.isShowing()) {
            currentDialog.dismiss();
            currentDialog = null;
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.trustline_fragment, container, false);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);

        addSpeedDialItem(
                R.id.add_trustline_manual,
                R.drawable.ic_edit,
                R.string.add_trustline_manual);

        addSpeedDialItem(
                R.id.add_trustline_common,
                R.drawable.ic_star,
                R.string.add_trustline_common);

        addSpeedDialItem(
                R.id.add_trustline_federation,
                R.drawable.ic_link,
                R.string.add_trustline_federation);

        speedDialView.setOnActionSelectedListener(this::onSpeedDialAction);

        initRecyclerView();
    }

    private void addSpeedDialItem(@IdRes int idRes,
                                  @DrawableRes int drawableRes,
                                  @StringRes int stringRes) {
        Resources resources = getResources();
        speedDialView.addActionItem(
                new SpeedDialActionItem.Builder(idRes, drawableRes)
                        .setFabBackgroundColor(resources.getColor(R.color.colorAccent))
                        .setLabel(resources.getString(stringRes))
                        .setLabelBackgroundColor(resources.getColor(R.color.colorPrimary))
                        .setLabelColor(resources.getColor(R.color.colorPrimaryText))
                        .create()
        );
    }

    private void initRecyclerView() {
        Context context = getContext();
        if (context == null) {
            return;
        }

        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new TrustlineRecyclerAdapter();
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
        ViewUtils.addDividerDecoration(recyclerView, context, layoutManager.getOrientation());
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    speedDialView.hide();
                } else if (dy < 0) {
                    speedDialView.show();
                }
            }
        });
    }

    @Nullable
    @Override
    public String getAccountId() {
        Bundle args = getArguments();
        return args == null ? null : args.getString(ACCOUNT_ID_ARG);
    }

    @Override
    public void showLoading(boolean isLoading) {
        ViewUtils.whenNonNull(getActivity(), activity ->
                ((TrustlineFlowManager) activity).showLoading(isLoading));
    }

    @Override
    public void showBlockedLoading(boolean isAddingTrust) {
        int messageResId = isAddingTrust ?
                R.string.saving_trustline_progress_message
                : R.string.removing_trustline_progress_message;
        dismissConfirmationFragment();
        ViewUtils.whenNonNull(getActivity(), activity ->
                ((TrustlineFlowManager) activity).showBlockedLoading(getString(messageResId)));
    }

    @Override
    public void hideBlockedLoading(boolean wasSuccess, boolean isAddingTrust) {
        int messageResId;
        if (wasSuccess) {
            messageResId = isAddingTrust ?
                    R.string.saving_trustline_progress_success
                    : R.string.removing_trustline_progress_success;
        } else {
            messageResId = isAddingTrust ?
                    R.string.saving_trustline_progress_fail
                    : R.string.removing_trustline_progress_fail;
        }

        ViewUtils.whenNonNull(getActivity(), activity ->
                ((TrustlineFlowManager) activity).hideBlockedLoading(
                        getString(messageResId),
                        wasSuccess,
                        false,
                        null));
    }

    @Override
    public void showTrustlines(@NonNull List<Trustline> currentTrusts) {
        if (currentTrusts.isEmpty()) {
            recyclerView.setVisibility(View.GONE);
            noTrustlinesMessage.setVisibility(View.VISIBLE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            noTrustlinesMessage.setVisibility(View.GONE);
            adapter.setTrustlines(currentTrusts);
        }
    }

    @Override
    public void showCommonTrustOptions(@NonNull List<CommonTrustline> commonTrustlines, double fee) {
        dismissCurrentDialog();
        CommonTrustlineAdapter adapter =
                new CommonTrustlineAdapter(activityContext, commonTrustlines);
        currentDialog = new AlertDialog.Builder(activityContext, R.style.DefaultAlertDialog)
                .setTitle(R.string.trustline_add_common_trust_title)
                .setAdapter(adapter, (dialog, which) ->
                        showTrustlineInputDialog(TrustlineConfirmationFragment
                                .newInstanceCommonAsset(adapter.getTrustlineAtPosition(which),
                                        fee)))
                .create();
        currentDialog.show();
    }

    @Override
    public void showManualInputConfirmation(double fee) {
        showTrustlineInputDialog(TrustlineConfirmationFragment.newInstanceManualInput(fee));
    }

    @Override
    public void showNoCommonTrustsLeft() {
        Toast.makeText(activityContext, R.string.all_common_trusts_trusted, Toast.LENGTH_SHORT)
                .show();
    }

    @Override
    public void showFederationUrlError(@Nullable String url) {
        Toast.makeText(activityContext, R.string.invalid_federation_url, Toast.LENGTH_SHORT).show();
        showFederationUrlDialog(url);
    }

    @Override
    public void showFederatedTrustlineOptions(@NonNull List<FederatedTrustlineChoice> trustlines) {
        dismissCurrentDialog();
        FederatedTrustlineAdapter adapter =
                new FederatedTrustlineAdapter(activityContext, trustlines);
        currentDialog = new AlertDialog.Builder(activityContext, R.style.DefaultAlertDialog)
                .setTitle(R.string.trustline_choose_federated_trustline_title)
                .setAdapter(adapter, (dialog, which) ->
                        presenter.onUserSelectedFederatedTrustline(
                                adapter.getTrustlineAtPosition(which)))
                .create();
        currentDialog.show();
    }

    @Override
    public void showTrustlineInputDialog(@NonNull FederatedTrustlineChoice trustlineChoice, double fee) {
        showTrustlineInputDialog(
                TrustlineConfirmationFragment.newInstanceFederatedTrustlineChoice(trustlineChoice, fee));
    }

    @Override
    public void showInsufficientFundsError(double minimumBalance) {
        dismissConfirmationFragment();
        Toast.makeText(
                activityContext,
                getString(R.string.trustline_creation_insufficient_funds,
                        AssetUtil.getAssetAmountString(minimumBalance)),
                Toast.LENGTH_SHORT)
                .show();
    }

    @Override
    public void showTrustlineRequestError(@NonNull TrustlineRequestError error) {
        if (error == TrustlineRequestError.UNKNOWN_ERROR) {
            dismissConfirmationFragment();
            Toast.makeText(
                    activityContext,
                    R.string.trustline_unknown_error_message,
                    Toast.LENGTH_SHORT)
                    .show();
            return;
        }

        ViewUtils.whenNonNull(getConfirmationFragment(), confirmationFragment ->
                confirmationFragment.showError(error));
    }

    @Override
    public void showRemoveTrustlineError(@NonNull TrustlineRemoveError trustlineRemoveError) {
        switch (trustlineRemoveError) {
            case PASSWORD_LENGTH:
                Toast.makeText(activityContext, R.string.password_length_error, Toast.LENGTH_SHORT)
                        .show();
                break;
            case BALANCE_NOT_ZERO:
                Toast.makeText(activityContext, R.string.trustline_balance_not_zero_message,
                        Toast.LENGTH_SHORT)
                        .show();
                break;
        }
    }

    @Override
    public void showRemoveTrustlineConfirmation(@NonNull Trustline trustline, double fee) {
        View dialogView = LayoutInflater.from(activityContext)
                .inflate(R.layout.remove_trustline_dialog_view,
                        (ViewGroup) getView(),
                        false);
        TextInputEditText editText = dialogView.findViewById(R.id.password_field);
        ((TextView) dialogView.findViewById(R.id.transaction_fee))
                .setText(FeesUtil.getFeeString(getResources(), fee));

        dismissCurrentDialog();
        currentDialog = new AlertDialog.Builder(activityContext, R.style.WarningAlertDialog)
                .setTitle(R.string.remove_trustline_title)
                .setView(dialogView)
                .setPositiveButton(R.string.remove, (((dialog, which) ->
                        presenter.onUserRequestRemoveTrust(trustline,
                                editText.getText().toString()))))
                .setNegativeButton(R.string.cancel, null)
                .create();

        currentDialog.show();
    }

    private void dismissConfirmationFragment() {
        ViewUtils.whenNonNull(getConfirmationFragment(), TrustlineConfirmationFragment::dismiss);
    }

    @Nullable
    private TrustlineConfirmationFragment getConfirmationFragment() {
        Fragment fragment = childFragmentManager
                .findFragmentByTag(TrustlineConfirmationFragment.class.getSimpleName());
        if (fragment == null) {
            Timber.e("Confirmation fragment was null");
        }

        if (fragment instanceof TrustlineConfirmationFragment) {
            return (TrustlineConfirmationFragment) fragment;
        } else {
            Timber.e("Tagged fragment was incorrect type");
        }

        return null;
    }

    @Override
    public void showRefreshFailedError() {
        Toast.makeText(activityContext, R.string.trustline_refresh_failure, Toast.LENGTH_SHORT)
                .show();
    }

    private boolean onSpeedDialAction(SpeedDialActionItem speedDialActionItem) {
        switch (speedDialActionItem.getId()) {
            case R.id.add_trustline_federation:
                showFederationUrlDialog(null);
                break;
            case R.id.add_trustline_common:
                presenter.onUserRequestAddCommonTrust();
                break;
            case R.id.add_trustline_manual:
                presenter.onUserRequestManualInput();
                break;
            default:
                Timber.e("Unrecognized speed dial action: %s",
                        speedDialActionItem.getLabel());
                break;
        }

        return false;
    }

    private void showTrustlineInputDialog(@NonNull TrustlineConfirmationFragment trustlineConfirmationFragment) {
        trustlineConfirmationFragment.show(childFragmentManager,
                TrustlineConfirmationFragment.class.getSimpleName());
    }

    private void showFederationUrlDialog(@Nullable String initialText) {
        dismissCurrentDialog();
        View dialogView = LayoutInflater.from(activityContext)
                .inflate(R.layout.federation_url_dialog_view,
                        (ViewGroup) getView(),
                        false);
        TextInputEditText editText = dialogView.findViewById(R.id.federation_url_field);
        editText.setText(initialText);


        currentDialog = new AlertDialog.Builder(activityContext, R.style.DefaultAlertDialog)
                .setTitle(R.string.trustline_input_federation_url_title)
                .setView(dialogView)
                .setPositiveButton(R.string.next, ((dialog, which) -> {
                    presenter.onUserInputFederationUrl(editText.getText().toString());
                }))
                .setNegativeButton(R.string.cancel, null)
                .create();

        editText.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                if (currentDialog != null) {
                    currentDialog.getButton(AlertDialog.BUTTON_POSITIVE).performClick();
                }
                return true;
            }

            return false;
        });

        currentDialog.show();
    }

    @Override
    public void onTrustlineConfirmed(@Nullable String issuer,
                                     @Nullable String assetCode,
                                     @Nullable String limit,
                                     @Nullable String password) {
        presenter.onUserRequestAddTrust(issuer, assetCode, limit, password);
    }

    @Override
    public void onUserRefreshed() {
        presenter.onUserRequestRefresh();
    }

    private class TrustlineRecyclerAdapter extends RecyclerView.Adapter<TrustlineViewHolder> {
        private final ArrayList<Trustline> trustlines = new ArrayList<>();

        @NonNull
        @Override
        public TrustlineViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new TrustlineViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.trustline_recycler_item, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull TrustlineViewHolder holder, int position) {
            holder.bindData(trustlines.get(position));
        }

        @Override
        public int getItemCount() {
            return trustlines.size();
        }

        public void setTrustlines(@Nullable List<Trustline> newTrustlines) {
            this.trustlines.clear();
            ViewUtils.whenNonNull(newTrustlines, nTL -> {
                trustlines.addAll(nTL);
                Collections.sort(trustlines, TRUSTLINE_COMPARATOR);
            });
            notifyDataSetChanged();
        }

        @NonNull
        public List<Trustline> getTrustlines() {
            return trustlines;
        }
    }

    class TrustlineViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener {
        @BindView(R.id.logo)
        ImageView logo;

        @BindView(R.id.asset_code)
        TextView assetCode;

        @BindView(R.id.issuer)
        TextView issuer;

        @BindView(R.id.trust_amount)
        TextView trustAmount;

        private Trustline trustline;

        TrustlineViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnLongClickListener(this);
        }

        void bindData(@NonNull Trustline trustline) {
            this.trustline = trustline;
            Integer logoResId = trustline.getLogoResId();
            if (logoResId == null) {
                logo.setVisibility(View.GONE);
            } else {
                logo.setVisibility(View.VISIBLE);
                logo.setImageDrawable(AppCompatResources.getDrawable(activityContext, logoResId));
            }

            assetCode.setText(trustline.getAssetCode());
            issuer.setText(trustline.getIssuer());
            trustAmount.setText(trustline.getLimitText());
        }

        @Override
        public boolean onLongClick(View v) {
            if (trustline == null) {
                Timber.e("Trustline was null during long press");
            } else {
                presenter.onUserRequestRemoveTrust(trustline);
            }
            return true;
        }
    }

    private static class FederatedTrustlineAdapter extends ArrayAdapter<FederatedTrustlineChoice> {
        @NonNull
        private final List<FederatedTrustlineChoice> trustlines;

        class ViewHolder {
            TextView assetCode;
            TextView assetName;
            TextView assetIssuer;
            View divider;
        }

        FederatedTrustlineAdapter(@NonNull Context context,
                                  @NonNull List<FederatedTrustlineChoice> trustlines) {
            super(context, R.layout.federation_trustline_choice, trustlines);
            this.trustlines = trustlines;
        }

        @Override
        @NonNull
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            FederatedTrustlineChoice trustline = getTrustlineAtPosition(position);

            ViewHolder holder;
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.federation_trustline_choice, null);
                holder = new ViewHolder();
                holder.assetCode = convertView.findViewById(R.id.asset_code);
                holder.assetName = convertView.findViewById(R.id.asset_name);
                holder.assetIssuer = convertView.findViewById(R.id.asset_issuer);
                holder.divider = convertView.findViewById(R.id.divider);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.assetCode.setText(trustline.assetCode);
            holder.assetName.setText(trustline.assetName);
            holder.assetIssuer.setText(trustline.assetIssuer);
            holder.divider.setVisibility(position != trustlines.size() - 1 ?
                    View.VISIBLE
                    : View.GONE);

            return convertView;
        }

        @NonNull
        FederatedTrustlineChoice getTrustlineAtPosition(int position) {
            return trustlines.get(position);
        }
    }

    private static class CommonTrustlineAdapter extends ArrayAdapter<CommonTrustline> {
        @NonNull
        private final List<CommonTrustline> commonTrustlines;

        class ViewHolder {
            View coloredBackground;
            ImageView icon;
            TextView text;
        }

        CommonTrustlineAdapter(@NonNull Context context,
                               @NonNull List<CommonTrustline> commonTrustlines) {
            super(context, R.layout.icon_text_item, commonTrustlines);
            this.commonTrustlines = commonTrustlines;
        }

        @Override
        @NonNull
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            CommonTrustline commonTrustline = getTrustlineAtPosition(position);

            ViewHolder holder;
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.icon_text_item, null);
                holder = new ViewHolder();
                holder.coloredBackground = convertView.findViewById(R.id.colored_background);
                holder.icon = convertView.findViewById(R.id.icon);
                holder.text = convertView.findViewById(R.id.item_text);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.coloredBackground.setBackgroundColor(commonTrustline.getColor());
            holder.text.setText(getContext().getString(R.string.common_trustline_asset_url_format,
                    commonTrustline.getAssetCode(), commonTrustline.getUrl()));
            holder.icon.setImageDrawable(AppCompatResources.getDrawable(getContext(),
                    commonTrustline.getLogoResId()));
            return convertView;
        }

        @NonNull
        CommonTrustline getTrustlineAtPosition(int position) {
            return commonTrustlines.get(position);
        }
    }
}
