package tech.duchess.luminawallet.view.account.send;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.InputFilter;
import android.text.SpannableStringBuilder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import butterknife.OnTextChanged;
import tech.duchess.luminawallet.R;
import tech.duchess.luminawallet.model.persistence.account.Account;
import tech.duchess.luminawallet.model.persistence.account.Balance;
import tech.duchess.luminawallet.model.persistence.contacts.Contact;
import tech.duchess.luminawallet.model.util.AssetUtil;
import tech.duchess.luminawallet.presenter.account.AccountsContract.AccountsView;
import tech.duchess.luminawallet.presenter.account.send.SendContract;
import tech.duchess.luminawallet.presenter.account.send.TransactionSummary;
import tech.duchess.luminawallet.view.account.AccountPerspectiveView;
import tech.duchess.luminawallet.view.common.BaseViewFragment;
import tech.duchess.luminawallet.view.contacts.ContactsActivity;
import tech.duchess.luminawallet.view.contacts.ContactsFlowManager;
import tech.duchess.luminawallet.view.util.TextUtils;
import tech.duchess.luminawallet.view.util.ViewUtils;

public class SendFragment extends BaseViewFragment<SendContract.SendPresenter>
        implements AccountPerspectiveView, SendContract.SendView,
        SendConfirmationFragment.TransactionConfirmationListener {
    private static final String ACCOUNT_KEY = "SendFragment.ACCOUNT_KEY";
    private static final String BALANCE_KEY = "SendFragment.BALANCE_KEY";
    private static final int SELECT_CONTACT_REQUEST_CODE = 1;
    private static final Comparator<Balance> BALANCE_COMPARATOR = ((o1, o2) -> {
       if (AssetUtil.isLumenBalance(o1)) {
           return -1;
       } else if (AssetUtil.isLumenBalance(o2)) {
           return 1;
       } else if (o1.getAsset_code().equalsIgnoreCase(o2.getAsset_code())) {
           return o1.getAsset_issuer().compareToIgnoreCase(o2.getAsset_issuer());
       } else {
           return o1.getAsset_code().compareToIgnoreCase(o2.getAsset_code());
       }
    });

    private static final InputFilter ASCII_FILTER = (source, start, end, dest, dstart, dend) -> {
        SpannableStringBuilder ret;

        if (source instanceof SpannableStringBuilder) {
            ret = (SpannableStringBuilder) source;
        } else {
            ret = new SpannableStringBuilder(source);
        }

        for (int i = end - 1; i >= start; i--) {
            char currentChar = source.charAt(i);
            if (currentChar > 127) {
                ret.delete(i, i + 1);
            }
        }

        return ret;
    };

    @BindView(R.id.recipient_field_layout)
    TextInputLayout recipientLayout;

    @BindView(R.id.recipient_field)
    TextInputEditText recipient;

    @BindView(R.id.amount_field_layout)
    TextInputLayout amountLayout;

    @BindView(R.id.amount_field)
    TextInputEditText amountField;

    @BindView(R.id.memo_field)
    TextInputEditText memoField;

    @BindView(R.id.unit_spinner)
    Spinner currencyUnitSpinner;

    @BindView(R.id.available_balance)
    TextView availableBalance;

    @BindView(R.id.issuer)
    TextView issuer;

    @Nullable
    private Balance lastSelectedBalance;

    @Nullable
    private BalanceAdapter balanceAdapter;

    private DialogFragment confirmationFragment;
    private Dialog curDialog;

    public static SendFragment newInstance(@Nullable Account account) {
        Bundle args = new Bundle();
        args.putParcelable(ACCOUNT_KEY, account);
        SendFragment fragment = new SendFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            lastSelectedBalance = savedInstanceState.getParcelable(BALANCE_KEY);
        }
        return inflater.inflate(R.layout.send_fragment, container, false);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        memoField.setFilters(new InputFilter[]{ASCII_FILTER});

        if (savedInstanceState == null) {
            Bundle args = getArguments();
            presenter.onAccountUpdated(args == null ? null : args.getParcelable(ACCOUNT_KEY));
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (confirmationFragment != null) {
            confirmationFragment.dismiss();
            confirmationFragment = null;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ViewUtils.whenNonNull(curDialog, Dialog::dismiss);
    }

    @Override
    public void setAccount(@Nullable Account account) {
        presenter.onAccountUpdated(account);
    }

    @Override
    public void transactionPostedForAccount(@NonNull Account account) {
        presenter.onAccountUpdated(account);
    }

    @OnClick(R.id.send_payment_button)
    public void onUserSendPayment() {
        recipientLayout.setError(null);
        amountLayout.setError(null);
        presenter.onUserSendPayment(recipient.getText().toString(),
                amountField.getText().toString(),
                getSelectedBalance(),
                memoField.getText().toString());
    }

    @Override
    public void showBlockedLoading(boolean isLoading,
                                   boolean isBuildingTransaction,
                                   boolean wasSuccess) {
        AccountsView accountsView = (AccountsView) activityContext;
        if (isLoading) {
            accountsView.showBlockedLoading(getString(isBuildingTransaction ?
                            R.string.loading_building_transaction
                            : R.string.loading_sending_payment));
        } else {
            int successResource;
            if (wasSuccess) {
                // We immediately hide overlay on success, since the transaction confirmation
                // dialog is coming.
                successResource = isBuildingTransaction ? 0 : R.string.send_payment_success;
            } else {
                successResource = isBuildingTransaction ? R.string.build_transaction_fail
                        : R.string.send_payment_fail;
            }

            String successMessage = successResource == 0 ? null : getString(successResource);
            accountsView.hideBlockedLoading(successMessage, wasSuccess,
                    isBuildingTransaction && wasSuccess);
        }
    }

    @Override
    public void showError(@NonNull SendContract.SendPresenter.SendError error) {
        new Handler(Looper.getMainLooper()).post(() -> {
            switch (error) {
                case ADDRESS_INVALID:
                    recipientLayout.setError(getString(R.string.account_address_format_error));
                    break;
                case DEST_SAME_AS_SOURCE:
                    recipientLayout.setError(getString(R.string.account_address_same_as_self_error));
                    break;
                case ADDRESS_DOES_NOT_EXIST:
                    recipientLayout.setError(getString(R.string.account_address_not_exist_error));
                    break;
                case ADDRESS_BAD_LENGTH:
                    recipientLayout.setError(getString(R.string.account_address_length_error,
                            getResources().getInteger(R.integer.address_length)));
                    break;
                case ADDRESS_BAD_PREFIX:
                    recipientLayout.setError(getString(R.string.account_address_prefix_error));
                    break;
                case ADDRESS_UNSUPPORTED_CURRENCY:
                    recipientLayout.setError(getString(R.string.account_address_unsupported_asset_error));
                    break;
                case INSUFFICIENT_FUNDS:
                    amountLayout.setError(getString(R.string.insufficient_funds_error));
                    break;
                case AMOUNT_GREATER_THAN_ZERO:
                    amountLayout.setError(getString(R.string.amount_greater_than_zero_error));
                    break;
                case SEQUENCE_HAS_CHANGED:
                    showSequenceChangedError();
                    break;
                case PASSWORD_INVALID:
                    Toast.makeText(activityContext, R.string.invalid_password_error, Toast.LENGTH_SHORT).show();
                    break;
            }
        });
    }

    private void showSequenceChangedError() {
        curDialog = new AlertDialog.Builder(activityContext, R.style.WarningAlertDialog)
                .setTitle(R.string.sequence_changed_title)
                .setMessage(R.string.sequence_changed_message)
                .setPositiveButton(R.string.sequence_changed_confirmation_button, null)
                .setCancelable(false)
                .create();
        curDialog.show();
    }

    @OnTextChanged(R.id.recipient_field)
    public void onRecipientContentsChanged(Editable editable) {
        recipientLayout.setError(null);
    }

    @OnTextChanged(R.id.amount_field)
    public void onAmountContentsChanged(Editable editable) {
        amountLayout.setError(null);
    }

    @OnItemSelected(R.id.unit_spinner)
    public void onCurrencyUnitChanged(Spinner spinner, int pos) {
        amountLayout.setError(null);
        lastSelectedBalance = getSelectedBalance();
        updateBalanceDetails();
    }

    @Override
    public void showConfirmation(@NonNull TransactionSummary transactionSummary) {
        confirmationFragment = SendConfirmationFragment.newInstance(transactionSummary);
        confirmationFragment.show(childFragmentManager,
                SendConfirmationFragment.class.getSimpleName());
    }

    @Override
    public void showNoAccount() {

    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(BALANCE_KEY, lastSelectedBalance);
    }

    @Override
    public void setAvailableCurrencies(@NonNull List<Balance> balances) {
        balanceAdapter = new BalanceAdapter(activityContext,
                R.layout.spinner_item, balances);
        balanceAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        currencyUnitSpinner.setAdapter(balanceAdapter);
        currencyUnitSpinner.setSelection(balanceAdapter.getSelectedIndex(lastSelectedBalance));
        updateBalanceDetails();
    }

    private void updateBalanceDetails() {
        Balance selectedBalance = getSelectedBalance();
        if (selectedBalance == null) {
            availableBalance.setText("");
            issuer.setText("");
            availableBalance.setVisibility(View.GONE);
            issuer.setVisibility(View.GONE);
        } else {
            availableBalance.setText(getString(R.string.available_balance_amount_prefix,
                    AssetUtil.getAssetAmountString(selectedBalance.getBalance())));
            issuer.setText(getString(R.string.available_balance_issuer_prefix,
                    selectedBalance.getAsset_issuer()));
            availableBalance.setVisibility(View.VISIBLE);

            if (balanceAdapter != null
                    && balanceAdapter.duplicateAssets.contains(selectedBalance.getAsset_code().toLowerCase())) {
                issuer.setVisibility(View.VISIBLE);
            } else {
                issuer.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void clearForm() {
        recipient.setText(null);
        amountField.setText(null);
        memoField.setText(null);
    }

    @Override
    public void showTransactionSuccess(@NonNull Account account, @NonNull String destination) {
        ((AccountsView) activityContext).onTransactionPosted(account, destination);
    }

    @Override
    public void onTransactionConfirmed(@Nullable String password) {
        presenter.onUserConfirmPayment(password);
    }

    @OnClick(R.id.take_picture)
    public void onUserRequestQRScanner() {
        IntentIntegrator intentIntegrator = IntentIntegrator.forSupportFragment(this);
        intentIntegrator.setBeepEnabled(false);
        intentIntegrator.setBarcodeImageEnabled(false);
        intentIntegrator.initiateScan();
    }

    @OnClick(R.id.pick_contact)
    public void onUserRequestPickContact() {
        startActivityForResult(ContactsActivity.createIntentForContactSelection(activityContext),
                SELECT_CONTACT_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SELECT_CONTACT_REQUEST_CODE
                && resultCode == Activity.RESULT_OK) {
            Contact selectedContact =
                    data.getParcelableExtra(ContactsFlowManager.CONTACT_SELECTION_RESULT_KEY);
            ViewUtils.whenNonNull(selectedContact, contact ->
                    recipient.setText(contact.getAddress()));
            return;
        }

        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (!TextUtils.isEmpty(result.getContents())) {
                recipient.setText(result.getContents());
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Nullable
    private Balance getSelectedBalance() {
        Object selectedItem = currencyUnitSpinner.getSelectedItem();
        return selectedItem == null ? null : (Balance) selectedItem;
    }

    private static class BalanceAdapter extends ArrayAdapter<Balance> {
        @NonNull
        private final List<Balance> balances;

        @NonNull
        private final Set<String> encounteredAssets = new HashSet<>();

        @NonNull
        private final Set<String> duplicateAssets = new HashSet<>();

        public BalanceAdapter(@NonNull Context context,
                              int resource,
                              @NonNull List<Balance> balances) {
            super(context, resource, balances);
            this.balances = balances;
            sort(BALANCE_COMPARATOR);

            encounteredAssets.clear();
            duplicateAssets.clear();
            for (Balance balance : balances) {
                String assetCode = balance.getAsset_code().toLowerCase();
                if (encounteredAssets.contains(assetCode)) {
                    duplicateAssets.add(assetCode);
                } else {
                    encounteredAssets.add(assetCode);
                }
            }
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            TextView label = (TextView) super.getView(position, convertView, parent);
            label.setText(balances.get(position).getAsset_code());
            // And finally return your dynamic (or custom) view for each spinner item
            return label;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            View v = super.getDropDownView(position, convertView, parent);
            TextView tv = ((TextView) v);
            tv.setText(balances.get(position).getAsset_code());
            return v;
        }

        public int getSelectedIndex(@Nullable Balance balance) {
            if (balance == null || balances.isEmpty()) {
                return 0;
            }

            for (int i = 0; i < balances.size(); i++) {
                Balance curBal = balances.get(i);
                if (curBal.getAsset_code().equals(balance.getAsset_code())
                        && curBal.getAsset_issuer().equalsIgnoreCase(balance.getAsset_issuer())) {
                    return i;
                }
            }

            return 0;
        }
    }
}
