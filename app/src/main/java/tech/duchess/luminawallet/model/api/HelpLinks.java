package tech.duchess.luminawallet.model.api;

public class HelpLinks {
    public static final String TRANSACTION_FEES =
            "https://www.stellar.org/developers/guides/concepts/fees.html#transaction-fee";
    public static final String MINIMUM_ACCOUNT_BALANCE =
            "https://www.stellar.org/developers/guides/concepts/fees.html#minimum-account-balance";
    public static final String INFLATION =
            "https://www.stellar.org/developers/guides/concepts/inflation.html";
    public static final String LUMENAUT =
            "https://lumenaut.net";
    public static final String TERMS_AND_CONDITIONS =
            "https://gitlab.com/duchesstech/LuminaWallet/wikis/Terms-and-Conditions-of-Use";
    public static final String PRIVACY_POLICY =
            "https://gitlab.com/duchesstech/LuminaWallet/wikis/Lumina-Privacy-Policy";
    public static final String SOURCE_CODE =
            "https://gitlab.com/duchesstech/LuminaWallet";
}
