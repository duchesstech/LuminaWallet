package tech.duchess.luminawallet;

/**
 * Constants used in Staging.
 */
public class EnvironmentConstants {
    public static final String HORIZON_API_ENDPOINT = "https://horizon-testnet.stellar.org";
    public static final String COIN_MARKET_CAP_API_ENDPOINT = "https://api.coinmarketcap.com/v1/";
    public static final boolean IS_PRODUCTION = false;
    public static final double BASE_FEE_PRECISION = 0.0000001f; // 1 Stroop
    public static final String LUMENAUT_INFLATION_ADDRESS = "GCCD6AJOYZCUAQLX32ZJF2MKFFAUJ53PVCFQI3RHWKL3V47QYE2BNAUT";
    public static final String STELLAR_EXPLORER_URL_PREFIX = "https://testnet.steexp.com/tx/";
    public static final String CONTACT_EMAIL = "lumina+debug@duchess.tech";
}